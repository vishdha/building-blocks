export const SUCCESS_MESSAGE = 'success';
export const INVALID_PASSWORD = 'Invalid password';
export const INVALID_TOKEN = 'Invalid Token';
export const TOKEN_NOT_FOUND = 'Token not found';
export const UNAUTHORIZED = 'Unauthorized';
